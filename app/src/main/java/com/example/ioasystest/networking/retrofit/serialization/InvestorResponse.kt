package com.example.ioasystest.networking.retrofit.serialization

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.*
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.*

val mapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class InvestorResponse (
    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val investor: Investor,

    val enterprise: Any? = null,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val success: Boolean
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<InvestorResponse>(json)
    }
}

data class Investor (
    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val id: Long,

    @get:JsonProperty("investor_name", required=true)@field:JsonProperty("investor_name", required=true)
    val investorName: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val email: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val city: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val country: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val balance: Long,

    val photo: Any? = null,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val portfolio: Portfolio,

    @get:JsonProperty("portfolio_value", required=true)@field:JsonProperty("portfolio_value", required=true)
    val portfolioValue: Long,

    @get:JsonProperty("first_access", required=true)@field:JsonProperty("first_access", required=true)
    val firstAccess: Boolean,

    @get:JsonProperty("super_angel", required=true)@field:JsonProperty("super_angel", required=true)
    val superAngel: Boolean
)

data class Portfolio (
    @get:JsonProperty("enterprises_number", required=true)@field:JsonProperty("enterprises_number", required=true)
    val enterprisesNumber: Long,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val enterprises: List<Any?>
)
