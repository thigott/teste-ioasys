package com.example.ioasystest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.ioasystest.R
import com.example.ioasystest.fragments.FragmentEnterpriseDetails
import com.example.ioasystest.fragments.FragmentSearch
import com.example.ioasystest.networking.retrofit.serialization.Enterprise
import com.example.ioasystest.viewmodel.CompanyViewModel
import kotlinx.android.synthetic.main.adapter_enterprises.view.*

class AdapterEnterprises(
    private val context: Context,
    private val companyVM: CompanyViewModel,
    private val activity: FragmentActivity
): RecyclerView.Adapter<AdapterEnterprises.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_enterprises, parent, false))
    }

    override fun getItemCount(): Int {
        return companyVM.enterpriseResponse?.enterprises!!.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val enterprise: Enterprise = companyVM.enterpriseResponse?.enterprises!![position]

        viewHolder.enterpriseName.text = enterprise.enterpriseName
        viewHolder.enterpriseType.text = enterprise.enterpriseType.enterpriseTypeName
        viewHolder.enterpriseCountry.text = enterprise.country.value

        viewHolder.enterpriseLayout.setOnClickListener {
            companyVM.enterpriseSelected = enterprise
            startFragmentEnterpriseDetails()
        }
    }

    private fun startFragmentEnterpriseDetails(){
        activity.supportFragmentManager.beginTransaction().setCustomAnimations(
            android.R.anim.fade_in, android.R.anim.fade_out,
            android.R.anim.fade_in, android.R.anim.fade_out
        ).replace(R.id.Activity_Main_Fragments, FragmentEnterpriseDetails(), "FragmentEnterpriseDetails")
            .addToBackStack(null).commit()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val enterpriseLayout: ConstraintLayout = itemView.Adapter_Enterprises_Layout
        val enterpriseLogo: ImageView = itemView.Adapter_Enterprises_Logo
        val enterpriseName: TextView = itemView.Adapter_Enterprises_Name
        val enterpriseType: TextView = itemView.Adapter_Enterprises_Type
        val enterpriseCountry: TextView = itemView.Adapter_Enterprises_Country
    }
}

