package com.example.ioasystest.viewmodel

import androidx.lifecycle.ViewModel
import com.example.ioasystest.networking.retrofit.serialization.CompanyLogin
import com.example.ioasystest.networking.retrofit.serialization.Enterprise
import com.example.ioasystest.networking.retrofit.serialization.EnterpriseReponse
import com.example.ioasystest.networking.retrofit.serialization.InvestorResponse

class CompanyViewModel : ViewModel() {
    var companyEmail: String? = null
    var companyPassword: String? = null

    var uid: String? = null
    var accessToken: String? = null
    var client: String? = null
    var investorResponse: InvestorResponse? = null

    var searchInput: String? = null

    var enterpriseResponse: EnterpriseReponse? = null
    var enterpriseSelected: Enterprise? = null
}