package com.example.ioasystest.networking.retrofit.serialization

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.*
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.*

data class EnterpriseReponse (
    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val enterprises: List<Enterprise>
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<EnterpriseReponse>(json)
    }
}

data class Enterprise (
    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val id: Long,

    @get:JsonProperty("email_enterprise")@field:JsonProperty("email_enterprise")
    val emailEnterprise: Any? = null,

    val facebook: Any? = null,
    val twitter: Any? = null,
    val linkedin: Any? = null,
    val phone: Any? = null,

    @get:JsonProperty("own_enterprise", required=true)@field:JsonProperty("own_enterprise", required=true)
    val ownEnterprise: Boolean,

    @get:JsonProperty("enterprise_name", required=true)@field:JsonProperty("enterprise_name", required=true)
    val enterpriseName: String,

    val photo: Any? = null,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val description: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val city: String,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val country: Country,

    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val value: Long,

    @get:JsonProperty("share_price", required=true)@field:JsonProperty("share_price", required=true)
    val sharePrice: Long,

    @get:JsonProperty("enterprise_type", required=true)@field:JsonProperty("enterprise_type", required=true)
    val enterpriseType: EnterpriseType
)

enum class Country(val value: String) {
    Argentina("Argentina"),
    Colombia("Colombia"),
    UnitedKingdom("United Kingdom"),
    UnitedStates("United States");

    companion object {
        fun fromValue(value: String): Country = when (value) {
            "Argentina"      -> Argentina
            "Colombia"       -> Colombia
            "United Kingdom" -> UnitedKingdom
            "United States"  -> UnitedStates
            else             -> throw IllegalArgumentException()
        }
    }
}

data class EnterpriseType (
    @get:JsonProperty(required=true)@field:JsonProperty(required=true)
    val id: Long,

    @get:JsonProperty("enterprise_type_name", required=true)@field:JsonProperty("enterprise_type_name", required=true)
    val enterpriseTypeName: String
)
