package com.example.ioasystest.networking.retrofit.service

import androidx.annotation.Keep
import com.example.ioasystest.networking.retrofit.serialization.CompanyLogin
import com.example.ioasystest.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

@Keep
interface ApiService {

    @POST("users/auth/sign_in")
    suspend fun postLogin(
        @Body companyLogin: CompanyLogin
    ): Response<ResponseBody>

    @GET("enterprises")
    suspend fun searchEnterprises(
        @Header("access-token") access_token: String,
        @Header("uid") uid: String,
        @Header("client") client: String,
        @QueryMap params: Map<String, String>
    ): Response<ResponseBody>

    companion object Factory {
        fun create(): ApiService {
            val okHttpClient = OkHttpClient()
                .newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build()
            val retrofit = Retrofit
                .Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}