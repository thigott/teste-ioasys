package com.example.ioasystest.networking.retrofit

import androidx.annotation.Keep
import com.example.ioasystest.fragments.FragmentHome
import com.example.ioasystest.fragments.FragmentSearch
import com.example.ioasystest.networking.retrofit.provider.DaggerServiceProviderComponent
import com.example.ioasystest.networking.retrofit.provider.ServiceProvider
import com.example.ioasystest.networking.retrofit.serialization.CompanyLogin
import dagger.Component
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Keep
class RequestHelper @Inject constructor(){
    @Inject
    lateinit var provider: ServiceProvider

    init {
        DaggerServiceProviderComponent.create().inject(this)
    }

    suspend fun postLogin(companyLogin: CompanyLogin): Response<ResponseBody>{
        return provider.postLogin(companyLogin)
    }

    suspend fun getSearchEnterprises(access_token: String, uid: String, client: String, params: Map<String, String>): Response<ResponseBody>{
        return provider.getSearchEnterprises(access_token, uid, client, params)
    }
}

@Keep
@Singleton
@Component
interface RequestHelperComponent {
    fun inject(app: FragmentHome)
    fun inject(app: FragmentSearch)
}