package com.example.ioasystest.networking.retrofit.provider

import androidx.annotation.Keep
import com.example.ioasystest.networking.retrofit.RequestHelper
import com.example.ioasystest.networking.retrofit.serialization.CompanyLogin
import com.example.ioasystest.networking.retrofit.service.ApiService
import dagger.Component
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Keep
class ServiceProvider @Inject constructor(){
    private val apiService = ApiService.create()

    suspend fun postLogin(companyLogin: CompanyLogin): Response<ResponseBody>{
        return apiService.postLogin(companyLogin)
    }

    suspend fun getSearchEnterprises(access_token: String, uid: String, client: String, params: Map<String, String>): Response<ResponseBody>{
        return apiService.searchEnterprises(access_token, uid, client, params)
    }
}

@Keep
@Singleton
@Component
interface ServiceProviderComponent{
    fun inject(app: RequestHelper)
}