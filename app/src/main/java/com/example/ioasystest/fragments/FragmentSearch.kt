package com.example.ioasystest.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.ioasystest.R
import com.example.ioasystest.networking.retrofit.DaggerRequestHelperComponent
import com.example.ioasystest.networking.retrofit.RequestHelper
import com.example.ioasystest.viewmodel.CompanyViewModel
import kotlinx.android.synthetic.main.fragment_search.*
import java.lang.Exception
import javax.inject.Inject
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ioasystest.adapters.AdapterEnterprises
import com.example.ioasystest.networking.retrofit.serialization.EnterpriseReponse
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentSearch : Fragment() {

    private lateinit var companyVM: CompanyViewModel

    @Inject
    lateinit var requestHelper: RequestHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onStart() {
        super.onStart()

        try {
            companyVM = ViewModelProviders.of(activity!!).get(CompanyViewModel::class.java)
            DaggerRequestHelperComponent.create().inject(this)
            setListeners()

            if (companyVM.enterpriseResponse != null) populateData()
        } catch (e: Exception){}
    }

    private fun setListeners(){
        Fragment_Search_Start_Button.setOnClickListener {
            Fragment_Search_Ioasys_Logo.visibility = View.GONE
            Fragment_Search_Start_Button.visibility = View.GONE
            Fragment_Search_Input.visibility = View.VISIBLE
            Fragment_Search_Instructions_Text.visibility = View.GONE
            Fragment_Search_Button.visibility = View.VISIBLE
        }

        Fragment_Search_Input.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= Fragment_Search_Input.right - Fragment_Search_Input.compoundDrawables[DRAWABLE_RIGHT].bounds.width()) {
                    Fragment_Search_Ioasys_Logo.visibility = View.VISIBLE
                    Fragment_Search_Start_Button.visibility = View.VISIBLE
                    Fragment_Search_Input.visibility = View.GONE
                    Fragment_Search_Enterprises_RecyclerView.visibility = View.GONE
                    Fragment_Search_Instructions_Text.visibility = View.VISIBLE
                    Fragment_Search_Button.visibility = View.GONE
                    Fragment_Search_Input.setText("")
                    shimmer_view_container.let {
                        it.visibility = View.GONE
                        it.stopShimmer()
                    }
                    Fragment_Search_Enterprises_RecyclerView.visibility = View.GONE
                    return@OnTouchListener true
                }
            }
            false
        })

        Fragment_Search_Button.setOnClickListener {
            it.hideKeyboard()
            shimmer_view_container.let { shimmer ->
                shimmer.visibility = View.VISIBLE
                shimmer.startShimmer()
            }
            searchEnterprises()
        }

        Fragment_Search_Input.addTextChangedListener { text -> companyVM.searchInput = text.toString() }
    }

    private fun searchEnterprises(){
        CoroutineScope(Dispatchers.Default + CoroutineName("EntreprisesSearch")).launch {
            try {
                val params: Map<String, String> = mapOf(
                    "name" to companyVM.searchInput!!
                )

                val res = requestHelper.getSearchEnterprises(
                    companyVM.accessToken!!,
                    companyVM.uid!!,
                    companyVM.client!!,
                    params
                )

                if (res.errorBody() == null){
                    companyVM.enterpriseResponse = res.body()!!.string().let { EnterpriseReponse.fromJson(it) }

                    if (!companyVM.enterpriseResponse?.enterprises.isNullOrEmpty()) {
                        this@FragmentSearch.activity?.runOnUiThread {
                            shimmer_view_container.let {
                                it.visibility = View.GONE
                                it.stopShimmer()
                            }
                            populateData()
                        }
                    }
                }
            } catch (e: Exception){
                this@FragmentSearch.activity?.runOnUiThread {
                    shimmer_view_container.let {
                        it.visibility = View.GONE
                        it.stopShimmer()
                    }
                    Fragment_Search_Instructions_Text.visibility = View.VISIBLE
                    Fragment_Search_Enterprises_RecyclerView.visibility = View.GONE
                    Fragment_Search_Instructions_Text.text = resources.getString(R.string.empty_list)
                }
            }
        }
    }

    private fun populateData(){
        if (companyVM.enterpriseResponse?.enterprises?.size!! > 0){
            Fragment_Search_Instructions_Text.text = resources.getString(R.string.search_instructions)
            Fragment_Search_Instructions_Text.visibility = View.GONE
            Fragment_Search_Start_Button.callOnClick()
            Fragment_Search_Enterprises_RecyclerView.let {
                it.layoutManager = LinearLayoutManager(activity)
                it.adapter = AdapterEnterprises(context!!, companyVM, activity!!)
                it.visibility = View.VISIBLE
            }
        } else {
            Fragment_Search_Instructions_Text.visibility = View.VISIBLE
            Fragment_Search_Enterprises_RecyclerView.visibility = View.GONE
            Fragment_Search_Instructions_Text.text = resources.getString(R.string.empty_list)
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
