package com.example.ioasystest.networking.retrofit.serialization

data class CompanyLogin(
    var email: String,
    var password: String
){
    fun toJson() = codec.writeValueAsString(this)
}