package com.example.ioasystest.utils

object Constants {
    private const val API_VERSION = "v1/"
    const val BASE_URL = "https://empresas.ioasys.com.br/api/${this.API_VERSION}"
}