package com.example.ioasystest.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.ioasystest.R
import com.example.ioasystest.networking.retrofit.DaggerRequestHelperComponent
import com.example.ioasystest.networking.retrofit.RequestHelper
import com.example.ioasystest.networking.retrofit.serialization.CompanyLogin
import com.example.ioasystest.networking.retrofit.serialization.InvestorResponse
import com.example.ioasystest.viewmodel.CompanyViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class FragmentHome : Fragment() {

    private lateinit var companyVM: CompanyViewModel

    @Inject
    lateinit var requestHelper: RequestHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onStart() {
        super.onStart()

        try {
            companyVM = ViewModelProviders.of(activity!!).get(CompanyViewModel::class.java)
            DaggerRequestHelperComponent.create().inject(this)
            setListeners()
        } catch (e: Exception){
            println(e)
        }
    }

    private fun setListeners(){
        Fragment_Home_Ioasys_Input_Email.addTextChangedListener { text -> companyVM.companyEmail = text.toString()
            Fragment_Home_Login_Response.visibility = View.GONE
        }
        Fragment_Home_Ioasys_Input_Password.addTextChangedListener { text -> companyVM.companyPassword = text.toString()
            Fragment_Home_Login_Response.visibility = View.GONE
        }

        Fragment_Home_Ioasys_Button_Login.setOnClickListener {
            Fragment_Home_Ioasys_Button_Login.text = resources.getString(R.string.loading)
            Fragment_Home_Ioasys_Button_Login.isClickable = false
            companyLogin()
        }
    }

    private fun companyLogin(){
        CoroutineScope(Dispatchers.Default + CoroutineName("CompanyLogin")).launch {
            try {
                val res = requestHelper.postLogin(CompanyLogin(
                    companyVM.companyEmail ?: "",
                    companyVM.companyPassword ?: ""
                ))

                if (res.errorBody() != null){
                    this@FragmentHome.activity?.runOnUiThread {
                        Fragment_Home_Login_Response.let {
                            it.visibility = View.VISIBLE
                            it.text = resources.getString(R.string.home_credenciais_invalidas)
                        }
                        Fragment_Home_Ioasys_Button_Login.text = resources.getString(R.string.login)
                        Fragment_Home_Ioasys_Button_Login.isClickable = true
                    }
                } else {
                    companyVM.uid = res.headers().get("uid")
                    companyVM.client = res.headers().get("client")
                    companyVM.accessToken = res.headers().get("access-token")
                    companyVM.investorResponse =
                        res.body()?.string()?.let { InvestorResponse.fromJson(it) }

                    Fragment_Home_Ioasys_Button_Login.text = resources.getString(R.string.login)
                    Fragment_Home_Ioasys_Button_Login.isClickable = true
                    startFragmentSearch()
                }

                println(res)
            } catch (e: Exception){
                println(e)
                this@FragmentHome.activity?.runOnUiThread {
                    Fragment_Home_Ioasys_Button_Login.text = resources.getString(R.string.login)
                    Fragment_Home_Ioasys_Button_Login.isClickable = true

                    Fragment_Home_Login_Response.let {
                        it.visibility = View.VISIBLE
                        it.text = resources.getString(R.string.home_credenciais_invalidas)
                    }
                    Fragment_Home_Ioasys_Button_Login.let {
                        it.text = resources.getString(R.string.home_button_log_in)
                        it.isClickable = true
                    }
                }
            }
        }
    }

    private fun startFragmentSearch(){
        activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(
            android.R.anim.fade_in, android.R.anim.fade_out,
            android.R.anim.fade_in, android.R.anim.fade_out
        )!!.replace(R.id.Activity_Main_Fragments, FragmentSearch(), "FragmentSearch")
            .addToBackStack(null).commit()
    }

}