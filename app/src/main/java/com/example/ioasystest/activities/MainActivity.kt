package com.example.ioasystest.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.ioasystest.R
import com.example.ioasystest.fragments.FragmentHome
import com.example.ioasystest.fragments.FragmentSearch
import com.example.ioasystest.viewmodel.CompanyViewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var companyVM: CompanyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        companyVM = ViewModelProviders.of(this).get(CompanyViewModel::class.java)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        Locale.setDefault(Locale("pt","BR"))
        startHomeFragment()
    }

    private fun startHomeFragment(){
        supportFragmentManager.beginTransaction()
            .add(R.id.Activity_Main_Fragments, FragmentHome())
            .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.Activity_Main_Fragments)) {
            is FragmentSearch -> {//the fragment on which you want to handle your back press
                this.moveTaskToBack(true)
            }
            else -> super.onBackPressed()
        }
    }
}
