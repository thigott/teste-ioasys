![N|Solid](logo_ioasys.png)

# README #

Aplicativo Android desenvolvido em Kotlin para o teste técnico Ioasys.

Arquitetura utilizada: MVVM
Bibliotecas utilizadas: 
	- Retrofit2: Para requisições web
	- Shimmer Facebook: Loading para a lista de dados
	- Coroutines

Com mais tempo seria interessante realizar a implementação de um cadastro para novas contas e novas funcionalidades tais como recuperação de senha e login via Google, Facebook.

Para executar a aplicação basta abri-la com o Android Studio.

Obrigado pela oportunidade.