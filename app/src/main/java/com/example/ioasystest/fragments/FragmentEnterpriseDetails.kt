package com.example.ioasystest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.ioasystest.R
import com.example.ioasystest.viewmodel.CompanyViewModel
import kotlinx.android.synthetic.main.fragment_enterprise_details.*

class FragmentEnterpriseDetails : Fragment() {

    private lateinit var companyVM: CompanyViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_enterprise_details, container, false)
    }

    override fun onStart() {
        super.onStart()

        try {
            companyVM = ViewModelProviders.of(activity!!).get(CompanyViewModel::class.java)

            Fragment_Enterprise_Details_Name.text = companyVM.enterpriseSelected?.enterpriseName
            Fragment_Enterprise_Details_Text.text = companyVM.enterpriseSelected?.description
            Fragment_Enterprise_Details_Back.setOnClickListener {
                activity!!.supportFragmentManager.popBackStack()
            }
        } catch (e: Exception){}
    }
}